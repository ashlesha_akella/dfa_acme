import functools

from absl import app
from absl import flags
import acme
from acme import wrappers
from acme.agents.tf import r2d2
from acme.tf import networks
import dm_env
import gym

flags.DEFINE_string('level', 'BeamRider-v0', 'Which Atari level to play.')
flags.DEFINE_integer('num_episodes', 1000, 'Number of episodes to train for.')

FLAGS = flags.FLAGS


def make_environment(evaluation: bool = False) -> dm_env.Environment:
    env = gym.make(FLAGS.level, full_action_space=True)

    max_episode_len = 108_000 if evaluation else 50_000

    return wrappers.wrap_all(env, [
        wrappers.GymAtariAdapter,
        functools.partial(
            wrappers.AtariWrapper,
            to_float=True,
            max_episode_len=max_episode_len,
            zero_discount_on_life_loss=True,
        ),
        wrappers.ObservationActionRewardWrapper,
        wrappers.SinglePrecisionWrapper,
    ])


def main(_):
    env = make_environment()
    env_spec = acme.make_environment_spec(env)

    network = networks.R2D2AtariNetwork(env_spec.actions.num_values)

    agent = r2d2.R2D2(env_spec, network, burn_in_length=20, trace_length=40, replay_period=1)

    loop = acme.EnvironmentLoop(env, agent)
    loop.run(FLAGS.num_episodes)


if __name__ == '__main__':
    app.run(main)
