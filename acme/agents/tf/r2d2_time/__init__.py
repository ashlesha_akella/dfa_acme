# Copyright 2018 DeepMind Technologies Limited. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Module for Recurrent DQN (R2D2)."""

import sys
sys.path.append('../../../../../')

from dfa_acme.acme.agents.tf.r2d2_time.agent import R2D2Time
from dfa_acme.acme.agents.tf.r2d2_time.learning import R2D2TimeLearner
