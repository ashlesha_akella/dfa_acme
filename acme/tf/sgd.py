import numpy as np


class SGD(object):

    def __init__(self, lr=1e-2, beta1=0.9, beta2=0.999, epsilon=1e-8):
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.epsilon = epsilon
        self.time = 0
        self.grads_first_moment = None
        self.grads_second_moment = None

    def update(self, layer, delta_weights, delta_bias):
        """Weights update using Adam.

          g1 = beta1 * g1 + (1 - beta1) * grads
          g2 = beta2 * g2 + (1 - beta2) * g2
          g1_unbiased = g1 / (1 - beta1**time)
          g2_unbiased = g2 / (1 - beta2**time)
          w = w - lr * g1_unbiased / (sqrt(g2_unbiased) + epsilon)
        """
        if 'time' not in layer.__dict__.keys():
            layer.time = 1
        if 'grads_first_moment' not in layer.__dict__.keys() and 'grads_second_moment' not in layer.__dict__.keys():
            layer.grads_first_moment = np.zeros_like(delta_weights)
            layer.grads_second_moment = np.zeros_like(delta_weights)
            layer.grads_first_moment_bias = np.zeros_like(delta_bias)
            layer.grads_second_moment_bias = np.zeros_like(delta_bias)

        layer.grads_first_moment = self.beta1 * layer.grads_first_moment + \
                                   (1. - self.beta1) * delta_weights
        layer.grads_second_moment = self.beta2 * layer.grads_second_moment + \
                                    (1. - self.beta2) * delta_weights ** 2

        self.grads_first_moment_unbiased = layer.grads_first_moment / (1. - self.beta1 ** layer.time)
        self.grads_second_moment_unbiased = layer.grads_second_moment / (1. - self.beta2 ** layer.time)

        layer.w = layer.w + - self.lr * self.grads_first_moment_unbiased / (
                np.sqrt(self.grads_second_moment_unbiased) + self.epsilon)

        layer.grads_first_moment_bias = self.beta1 * layer.grads_first_moment_bias + \
                                   (1. - self.beta1) * delta_bias
        layer.grads_second_moment_bias = self.beta2 * layer.grads_second_moment_bias + \
                                    (1. - self.beta2) * delta_bias ** 2

        self.grads_first_moment_unbiased_bias = layer.grads_first_moment_bias / (1. - self.beta1 ** layer.time)
        self.grads_second_moment_unbiased_bias = layer.grads_second_moment_bias / (1. - self.beta2 ** layer.time)

        layer.b = layer.b + -self.lr * self.grads_second_moment_unbiased_bias / (
                    np.sqrt(self.grads_second_moment_unbiased_bias) + self.epsilon)
        layer.b = layer.b + -self.lr
        layer.time += 1

        return layer
