import numpy as np
import tensorflow as tf


class SoftMax(object):

    def __init__(self):
        pass

    def __call__(self, x):
        exp = np.exp(x - np.max(x))
        return tf.convert_to_tensor(exp / np.sum(exp, axis=0, keepdims=True))

    def gradient(self, x):
        return tf.convert_to_tensor(x * (1 - x))


class Tanh(object):

    def __init__(self):
        pass

    def __call__(self, x):
        return tf.convert_to_tensor(np.tanh(x))

    def gradient(self, x):
        return tf.convert_to_tensor(1 - np.power(x, 2))


class Sigmoid(object):

    def __init__(self):
        pass

    def __call__(self, x):
        return tf.convert_to_tensor(1.0 / (1.0 + np.exp(-x)))

    def gradient(self, x):
        return tf.convert_to_tensor(x * (1 - x))


class Relu(object):

    def __init__(self, upper_limit=None):
        self.upper_limit = upper_limit

    def __call__(self, x):
        if self.upper_limit is None:
            return tf.maximum(0, x)
        else:
            return tf.convert_to_tensor(np.clip(np.maximum(0, x), a_min=0, a_max=self.upper_limit))

    def gradient(self, x):
        x[x > 0] = 1
        x[x < 0] = 0
        return tf.convert_to_tensor(x)


@tf.custom_gradient
def relu(x):
    x = tf.maximum(0, x)

    def grad(dy):
        x[x > 0] = 1
        x[x < 0] = 0
        return x

    return x, grad
