import torch
import torch.distributions as tdst
from acme.tf.networks.SNN.itconfig import device, alpha

torch.autograd.set_detect_anomaly(True)


class Synapses(object):

    def __init__(self, size):
        _weights = tdst.Normal(0.3, 0.2).sample(size)
        self.synapses = [[Synapse(_weights[i, j]) for j in range(size[1])] for i in range(size[0])]


class Synapse(object):
    """
    connection between 2 neurons
    current time step I[n+1] = (alpha*prev time I[n]) + weighted sum (prev timestep)
    alpha =decay rate
    """

    @staticmethod
    def current(weighted_sum, interconnection_weights=[]):
        activity = torch.zeros_like(weighted_sum, device=device)
        batch_size, timesteps, neurons = weighted_sum.shape
        if len(interconnection_weights) != 0:
            current = Synapse._activity_interconnections(activity, weighted_sum, timesteps, interconnection_weights)
        else:
            current = Synapse._activity(activity, weighted_sum, timesteps)
        return current

    @staticmethod
    def _activity(activity, weighted_sum, timesteps):
        # loop for every time step
        for i in range(timesteps - 1):
            activity[:, i + 1, :] = (alpha * activity[:, i, :]) + weighted_sum[:, i, :]
        return activity

    @staticmethod
    def _activity_interconnections(activity, weighted_sum, timesteps, interconnection_weights):
        interconnection_weights = interconnection_weights.to(device)
        for i in range(timesteps - 1):
            activity[:, :, i + 1] = (alpha * activity[:, :, i]) + (
                    weighted_sum[:, :, i] + torch.einsum("ab,bc-> ac", activity[:, :, i], interconnection_weights))
        return activity
