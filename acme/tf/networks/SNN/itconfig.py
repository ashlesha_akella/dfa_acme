import numpy as np
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# neuron
Resistance = 1
DELTA_TIME = 1e-3
voltage_threshold = 1
resting_potential = 0
tau_m = 10 * 1e-3  # membrane time constant
tau_syn = 5e-3
tau_d = 2e-3
tau_r = 3e-3
tau_ref = 2
alpha = float(np.exp(-DELTA_TIME/tau_syn))
beta = float(np.exp(-DELTA_TIME/tau_m))
bias_current = 0.005
