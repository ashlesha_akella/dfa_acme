import sys
sys.path.append('../../../../')
import matplotlib.pyplot as plt
import torch
from dfa_acme.acme.tf.networks.SNN.itconfig import *

"""
Neuron contains class for

LIF neuron

"""

TIMESTEPS = 1000


class Neuron(object):

    def __init__(self):
        pass


class SimplifiedLIF(Neuron):
    def __init__(self):
        super(SimplifiedLIF).__init__()
        self.type = None
        self.synapses = []
        self.spike_grad = Spike.apply
        self.beta = beta

    def voltage(self, current, timesteps=TIMESTEPS):
        u = torch.zeros(timesteps)
        _u = resting_potential
        u[0] = _u
        for t in range(timesteps - 1):
            if _u >= voltage_threshold:
                _u = resting_potential
            else:
                _u = self.beta * _u + Resistance * current[t]
            u[t + 1] = _u
        return torch.FloatTensor(u)

    def spike_activity(self, current, timesteps=TIMESTEPS):
        activity = torch.zeros(timesteps)
        _u = resting_potential
        for t in range(1, timesteps):
            if _u >= voltage_threshold:
                activity[t] = 1
                _u = resting_potential
            else:
                _u = self.beta * _u + Resistance * current[t]
        return self.spike_grad(activity)

    def set_type(self, type):
        self.type = type

    def plot_voltage(self, u):
        plt.plot(u.detach().numpy())
        plt.show()


class LIF(Neuron):

    def __init__(self):
        super(LIF).__init__()
        self.type = None
        self.synapses = []
        self.spike_grad = Spike.apply

    def voltage(self, current, timesteps=TIMESTEPS):
        u = torch.zeros(timesteps)
        _u = resting_potential
        u[0] = _u
        for t in range(timesteps - 1):
            if _u >= voltage_threshold:
                _u = resting_potential
            else:
                _u += ((-(_u - resting_potential) + (Resistance * current[t])) * DELTA_TIME) / tau_m
            u[t + 1] = _u
        return torch.FloatTensor(u)

    def spike_activity(self, current, timesteps=TIMESTEPS):
        activity = torch.zeros(timesteps)
        _u = resting_potential
        for t in range(1, timesteps):
            if _u >= voltage_threshold:
                activity[t] = 1
                _u = resting_potential
            else:
                _u += ((-(_u - resting_potential) + (Resistance * current[t])) * DELTA_TIME) / tau_m
        return activity

    def set_type(self, type):
        self.type = type

    def plot_voltage(self, u):
        plt.plot(u.detach().numpy())
        plt.show()


class LIFNeurons(object):

    def __init__(self, timesteps, n_neurons=0, id=None):
        self.n_neurons = n_neurons
        self.id = id
        self.spike_grad = Spike.apply
        self.timesteps = timesteps

    def spike_activity(self, current):
        batch, timesteps, neurons = current.shape
        voltage = torch.zeros_like(current, device=device)
        activity = torch.zeros_like(current, device=device)
        spike_activity = torch.zeros_like(current, device=device)
        _voltage = torch.ones((batch, neurons), device=device) * resting_potential
        for t in range(timesteps):
            voltage[:, t, :] = _voltage
            threshold_exceeded = torch.where(_voltage >= voltage_threshold)
            _voltage += ((-(_voltage - resting_potential) + (Resistance * current[:, t, :])) * DELTA_TIME) / tau_m
            _voltage[threshold_exceeded] = resting_potential
            spike_activity[:, t, :] = voltage[:, t, :]
            if t != 0 and t < timesteps-1:
                activity[:, t + 1, :] = (alpha * activity[:, t, :]) + spike_activity[:, t, :]
        spike_activity = self.spike_grad(voltage)
        return spike_activity, activity


class Spike(torch.autograd.Function):
    scale = 100.0

    @staticmethod
    def forward(ctx, input):
        ctx.save_for_backward(input)
        out = torch.zeros_like(input)
        out[input >= voltage_threshold] = 1.0
        return out

    @staticmethod
    def backward(ctx, grad_output):
        input = ctx.saved_tensors
        input = input[0]
        grad_input = grad_output.clone()
        grad = grad_input / (Spike.scale * torch.abs(input) + 1.0) ** 2
        return grad


if __name__ == '__main__':
    n = LIF()
    I = np.random.rand(TIMESTEPS) * 2
    l = LIFNeurons(n_neurons=1)
    # I = np.ones(TIMESTEPS)*2
    u = n.voltage(I)
    plt.plot(u)
    plt.show()
    s = l.spike_activity(torch.FloatTensor(I.reshape(1, TIMESTEPS, 1)))
    print("Sdfadf")
