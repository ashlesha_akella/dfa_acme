import sys

sys.path.append('../../../../../')
import time
import torch
import numpy as np
from torch import nn
from torch import optim
from itertools import groupby
from matplotlib import pyplot as plt

import tensorflow as tf
from dfa_acme.acme.tf.networks.SNN.itconfig import *
from dfa_acme.acme.tf.networks.SNN.neuron import LIFNeurons, Spike


class SpikeNetwork(object):
    """
        Builds Fully connected Feedforward network with random weights
        given the number of neurons in each layer.
    """

    def __init__(self, layers, weight_std=None, weight_mean=0.0, spike_input=False, log_spike_count=False,
                 learning_rate=None, constant_weight=False, drop_out=None, spike_output=False, timesteps=100):
        """

        :param layers: list of snt layers

        :param weight_std: weights are initialised using normal distribution with mean 0 and std = weight_std.
                            if weight_std is None, weight_std will default to
                            weight_scale/ sqrt(number of neurons in the next layer)

        :param spike_input: Boolean. True if the network input is a Spike train
                                    False if the network input is current.

        :param log_spike_count: Boolean True logs the spike count of each layer during forward pass.
                                It doesnt log when forawrd pass is used for a batch input

        :param learning_rate: Float. (Default 1e-3)
        :param constant_weight: Boolean, When True all weight will be initialized to weight_mean (constant value)
        :param drop_out: float, percentage of neurons to be ignored during training (example: 0.2)
        """

        self.spike_input = spike_input
        self.spike_output = spike_output
        self.activation = nn.Softmax(dim=1)
        self.drop_out = nn.Dropout(p=drop_out or 0.1)
        self.loss = nn.MSELoss()
        self.layers = layers
        # self.optimizer = torch.optim.Adam(self.weights, lr=learning_rate or 1e-3)
        self.spike = Spike.apply
        self.layer_spike_activity = []
        self.log_spike_count = log_spike_count
        params = []
        for i in self.layers:
            if 'activation' in i.__module__ or getattr(i, 'name', None) == 'activation':
                continue
            params.append(i.bias)
            params.append(i.weight)
        self.trainable_variables = params
        self.optimizer = optim.Adam(self.trainable_variables, lr=learning_rate or 1e-3)
        self.output_size = self.layers[-1].out_features
        # construct LIF layers
        self.LIF_layers = []
        for l in self.layers:
            self.LIF_layers.append(LIFNeurons(timesteps))
        if weight_mean is not None and weight_std is not None:
            self._init_weights(weight_mean=weight_mean, weight_std=weight_std)
        self.timesteps = timesteps

    def set_environment(self, timesteps, max_val, min_val):
        self.timesteps = timesteps
        self.max_val = max_val
        self.min_val = min_val

    def _init_weights(self, weight_mean, weight_std):
        for l in self.layers:
            if 'activation' in l.__module__ or getattr(l, 'name', None) == 'activation':
                continue
            torch.nn.init.normal_(l.weight, mean=weight_mean, std=weight_std)

    def set_inter_connection(self, layer, weight):
        """

        :param layer:
        :return: None
        """
        self.weights_within_layer[layer.id] = torch.ones((layer.n_neurons, layer.n_neurons)) * weight
        np.fill_diagonal(self.weights_within_layer[layer.id].detach().numpy(), 0)

    def _get_spike_state(self, firing_rate):
        # uni_dist = np.random.uniform(0, 1, (1, self.timesteps))
        # fr_limit = self.wall_fr * self.dt
        # self.wall_spike = 1 * (uni_dist < fr_limit)
        raise NotImplementedError

    def create_batch(self, X):
        X = X.numpy()
        if len(X.shape) == 2:
            _, time = X.shape
            X = X.reshape(1, X.shape[0], -1)
            output_size = (self.output_size, time)
        elif len(X.shape) == 3:
            batch, _, time = X.shape
            output_size = (batch, self.output_size, time)
        elif len(X.shape) == 4:  # it is an image input
            pass
        return torch.FloatTensor(X), output_size

    def __call__(self, input_x):
        # input_x, _ = self.create_batch(input_x)
        input_x = torch.FloatTensor(input_x.numpy().transpose(0, 3, 1, 2))
        for i, layer in enumerate(self.layers):
            input_x = layer(input_x)  # weighted sum of the input current
            if i < len(self.layers) - 1:
                _, input_x = self.LIF_layers[i].spike_activity(input_x)
        return input_x

    def train(self, input_x, output_x, epochs=2000):
        for i in range(epochs):
            start = time.time()
            actual = self(input_x)
            end = time.time()
            print('{} seconds '.format(end - start))
            loss = self.loss(actual, output_x)
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            # if i % 10 == 0:
            print('Loss {}'.format(loss.item()))

    def _log_spike_count(self, data, text_preprend=''):
        """

        :param data: is a 3D matrx (BATCH x NEURONS x TIMESTEPS
        :return: logs the spike count

        Ignore Logging if there are multiple batches
        """
        if data.shape[0] > 1:
            return
        for _batch in data:
            print("{} Spike count of {} neurons: {}".format(text_preprend, _batch.shape[0],
                                                            self.spike_count(_batch.detach().cpu().numpy())))

    def plot_layer_spike_activity(self, layer_spike_activity, filename):
        fig, axes = plt.subplots(len(layer_spike_activity[0]))
        for i, l in enumerate(layer_spike_activity[0]):
            axes[i].eventplot(l)
            axes[i].set_title("layer {}".format(i))
        plt.savefig('analysis/layer_spike/{}.jpeg'.format(filename))
        plt.close()

    @staticmethod
    def spike_count(data):
        """ data is 2D matrix """
        return [len(np.where(item == 1)[0]) for item in data]

    @staticmethod
    def spike_events(data):
        """ data is a 2D matrix """
        return [[k[1] for k in i[1]] for i in groupby(zip(*np.where(data == 1)), lambda x: x[0])]


if __name__ == '__main__':
    input_size = 2
    TIMESTEPS = 2000
    output_size = 1
    network = SpikeNetwork(layers=[nn.Linear(2, 200),
                                   nn.Linear(200, 50),
                                   nn.Linear(50, output_size)],
                           spike_input=False, weight_std=0.3, weight_mean=0.1, learning_rate=1e-4, constant_weight=True)
    input_current = torch.stack(
        [torch.cat([torch.ones(size=(TIMESTEPS - 200, 1)), torch.zeros(size=(200, 1))]) for i in
         range(input_size)]).reshape(
        input_size, TIMESTEPS).to(device).T

    a = np.linspace(-10, 10, 2000)
    expected = torch.sin(torch.FloatTensor(a)).reshape(1, 2000, 1)

    network.train(input_current, expected, epochs=3000)
