import torch
import pickle
import numpy as np
from copy import deepcopy
from matplotlib import pyplot as plt

USE_CUDA = True
device = torch.device(
    "cuda" if USE_CUDA and torch.cuda.is_available() and 'GeForce' not in torch.cuda.get_device_name(0) else "cpu")
#device = 'cpu'

TRAIN_RECURRENT_NEURONS = False
TRAIN_OUTPUT_NEURONS = True
RESTING_STATE_TIMESTEPS = 100
MIN_RNN_CLIP = -0.4
MAX_RNN_CLIP = 0.5
MIN_OUTPUT_CLIP = -0.4
MAX_OUTPUT_CLIP = 0.8


class RNNNetwork(object):

    def __init__(self, input_size, recurrent_units, output_size, connection_probability=0.8,
                 learning_rate=1e-4, percentage_of_recurrent_neurons_to_train=100):

        self.input_size = input_size
        self.recurrent_units = recurrent_units
        self.output_size = output_size
        self.connection_prob = connection_probability
        self.tau = 25  # ms
        self.gain_of_network = 5
        self.state_of_RNN = np.zeros((self.recurrent_units, self.recurrent_units))
        self.input_weights = []
        self.recurrent_weights = []
        self.output_weights = []
        self.time_step = 1
        self.alpha = learning_rate
        self.train_dt = 20
        self.bias_current = 1e-4
        self.error_plt = 0
        self.threshold = 0.4
        self.set_weights()
        self.percentage_of_recurrent_neurons_to_train = percentage_of_recurrent_neurons_to_train
        self.train_rnn_neurons = torch.randperm(self.recurrent_units)[
                                 :int(self.recurrent_units * (self.percentage_of_recurrent_neurons_to_train / 100))]

    def set_weights(self):
        # self.input_weights = torch.empty((self.input_size, self.recurrent_units), device=device, dtype=torch.float,
        #                                  requires_grad=False)
        self.input_weights = torch.ones((self.input_size, self.recurrent_units), device=device, dtype=torch.float,
                                        requires_grad=False) * 0.1
        torch.nn.init.normal_(self.input_weights, 0, 2)
        input_weight_mask = np.zeros(shape=(self.input_size, self.recurrent_units))
        size_col_to_mask = int(self.recurrent_units / self.input_size)

        for i in range(self.input_size):
            if i == 0:
                input_weight_mask[i, i * size_col_to_mask: size_col_to_mask + (i * size_col_to_mask)] = 1
            else:
                input_weight_mask[i, 1 + (i * size_col_to_mask): size_col_to_mask + (i * size_col_to_mask)] = 1

        self.input_weights = self.input_weights * torch.tensor(input_weight_mask, device=device, dtype=torch.float)
        self.input_weights = self.input_weights.T

        # recurrent
        self.recurrent_weights = torch.empty((self.recurrent_units, self.recurrent_units), device=device,
                                             dtype=torch.float, requires_grad=False)
        std_rec = 1 / np.sqrt(self.gain_of_network * self.recurrent_units)
        torch.nn.init.normal_(self.recurrent_weights, 0, std_rec)
        recurrent_weight_mask = np.ones(shape=(self.recurrent_units, self.recurrent_units))
        recurrent_weight_mask[recurrent_weight_mask < (1 - self.connection_prob)] = 0
        self.recurrent_weights = self.recurrent_weights * torch.tensor(recurrent_weight_mask, device=device,
                                                                       dtype=torch.float) * self.gain_of_network
        self.recurrent_weights[1: (self.recurrent_units + 1): self.recurrent_units * self.recurrent_units] = 0
        # output
        self.output_weights = torch.empty((self.output_size, self.recurrent_units), device=device, dtype=torch.float,
                                          requires_grad=False)
        self.output_weights = torch.nn.init.normal_(self.output_weights, 0, 1 / np.sqrt(self.recurrent_units))
        self.refresh()

    def refresh(self):
        self.recurrent_state = torch.zeros(size=(self.recurrent_units, 1), device=device) + self.bias_current
        self.recurrent_neurons_firing_rate = torch.zeros((self.recurrent_units, 1), device=device)
        self.output_signal = []
        self.activity_index = 0

    def forward(self, input_signal, timesteps):
        input_signal = input_signal.T
        input_signal = input_signal.to(device)
        recurrent_state_per_time_step = torch.zeros(size=(self.recurrent_units, timesteps),
                                                    device=device)
        recurrent_state = torch.zeros(size=(self.recurrent_units, 1), device=device) + self.bias_current
        output_signal = torch.zeros(size=(timesteps, self.output_size), device=device)
        recurrent_neurons_firing_rate = torch.zeros((self.recurrent_units, 1), device=device)
        for t in range(timesteps):
            R = torch.matmul(self.recurrent_weights, recurrent_neurons_firing_rate)
            I = torch.matmul(self.input_weights, input_signal[t, :]).view(-1, 1)
            recurrent_state = ((recurrent_state * (1 - (1 / self.tau))) + ((1 / self.tau) * (R + I)))
            recurrent_neurons_firing_rate = torch.tanh(recurrent_state)
            recurrent_state_per_time_step[:, t] = recurrent_neurons_firing_rate.view(-1)
            output_signal[t] = torch.matmul(self.output_weights, recurrent_neurons_firing_rate).view(-1)
        return output_signal, recurrent_state_per_time_step

    def get_time(self, input_signal, timesteps, with_limit=False):
        input_signal = torch.FloatTensor(input_signal).T
        input_signal = input_signal.to(device)
        output_signal = torch.zeros(size=(timesteps, self.output_size), device=device)
        for t in range(timesteps):
            R = torch.matmul(self.recurrent_weights, self.recurrent_neurons_firing_rate)
            I = torch.matmul(self.input_weights, input_signal[t, :]).view(-1, 1)
            self.recurrent_state = ((self.recurrent_state * (1 - (1 / self.tau))) + ((1 / self.tau) * (R + I)))
            self.recurrent_neurons_firing_rate = torch.tanh(self.recurrent_state)
            output_signal[t] = torch.matmul(self.output_weights, self.recurrent_neurons_firing_rate).view(-1)
            if all(output_signal[t - 10 : t].view(-1) >= self.threshold) and t > 20 and with_limit:
                output_signal = output_signal[:t, :]
                break
        time_to_act = (output_signal.view(-1) >= self.threshold).nonzero().view(-1).detach().cpu()
        self.output_signal.append(output_signal)
        if len(time_to_act) > 0:
            # time_to_observe = np.random.choice(time_to_act[:limit])
            return time_to_act[-1].item()
        else:
            # time_to_observe = np.random.choice(np.arange(TOTAL_TIMESTEPS)[-limit:])
            return int(timesteps / 2)

    def recurrent_neuron_activity(self, input_signal, timesteps):
        input_signal = input_signal.T
        input_signal = input_signal.to(device)
        recurrent_state_per_time_step = torch.zeros(size=(self.recurrent_units, timesteps), device=device)
        recurrent_state = torch.zeros(size=(self.recurrent_units, 1), device=device) + self.bias_current
        recurrent_neurons_firing_rate = torch.zeros((self.recurrent_units, 1), device=device)
        output_signal = torch.zeros(size=(timesteps, self.output_size), device=device)
        for t in range(timesteps):
            R = torch.matmul(self.recurrent_weights, recurrent_neurons_firing_rate)
            I = torch.matmul(self.input_weights, input_signal[t, :]).view(-1, 1)
            recurrent_state = (recurrent_state * (1 - (1 / self.tau)) + ((1 / self.tau) * (R + I)))
            recurrent_neurons_firing_rate = torch.tanh(recurrent_state)
            recurrent_state_per_time_step[:, t] = recurrent_neurons_firing_rate.view(-1)
            output_signal[t] = torch.einsum("ab,bc->ac", self.output_weights, recurrent_neurons_firing_rate).view(-1)
        return recurrent_state_per_time_step

    def target_output_and_rnn_activity(self, input_signal, activity, rnn_activity, timepoint, rewards):
        output_activity = deepcopy(activity.view(-1))
        rnn_activity -= 0.0001
        # output_activity = torch.zeros_like(activity.view(-1))
        # recurrent_neuron_activity = self.recurrent_neuron_activity(input_signal, activity.shape[0])
        for i, _t in enumerate(timepoint):
            _r = -0.01 if rewards[i] == 0 else rewards[i]
            output_activity[_t] += _r
            rnn_activity[:, _t] += _r
        return output_activity.view(-1, 1), rnn_activity

    def _add_resting_activity(self, recurrent_neuron_output, _activity, input_signal):
        recurrent_neuron_output = torch.cat(
            (recurrent_neuron_output, torch.zeros((self.recurrent_units, RESTING_STATE_TIMESTEPS), device=device)), 1)

        input_signal = torch.cat((input_signal.view(-1), torch.zeros(RESTING_STATE_TIMESTEPS, device=device))).view(
            self.input_size, -1)

        _activity = torch.cat((_activity.view(-1), torch.zeros(RESTING_STATE_TIMESTEPS))).view(-1, 1)

        return recurrent_neuron_output, _activity, input_signal
    
    def get_full_activity(self, save_acitivity_path, states, actions):
        combined_state = torch.zeros(self.input_size, sum(actions) + 1)
        combined_state = combined_state.to(device)
        timepoints = [0]
        start = 0
        states = torch.FloatTensor(states)
        for i, a in enumerate(actions):
            combined_state[:, start: start + a] = states[i][:, :a]
            start += a
            timepoints.append(a)
        timesteps = combined_state.shape[1]
        activity, rnn_activity = self.forward(combined_state, combined_state.shape[1])
        return activity

    def train_rnn(self, states, actions, rewards, index=None, iteration=3):
        combined_state = torch.zeros(self.input_size, sum(actions) + 1)
        combined_state = combined_state.to(device)
        timepoints = [0]
        start = 0
        states = torch.FloatTensor(states)
        for i, a in enumerate(actions):
            combined_state[:, start: start + a] = states[i][:, :a]
            start += a
            timepoints.append(a)
        timesteps = combined_state.shape[1]
        activity, rnn_activity = self.forward(combined_state, combined_state.shape[1])
        output_target, rnn_target = self.target_output_and_rnn_activity(combined_state, activity, rnn_activity,
                                                                        timepoints[1:], rewards)
        output_target = torch.clamp(output_target, max=MAX_OUTPUT_CLIP, min=MIN_OUTPUT_CLIP)
        rnn_target = torch.clamp(rnn_target, min=MIN_RNN_CLIP, max=MAX_RNN_CLIP)
        for _ in range(iteration):
            self.refresh()
            self.P = torch.zeros((self.recurrent_units, self.recurrent_units), device=device)
            self.P[self.train_rnn_neurons, self.train_rnn_neurons] = self.alpha
            self.P_out = torch.eye(self.recurrent_units, device=device) * self.alpha
            recurrent_state = torch.zeros(size=(self.recurrent_units, 1), device=device) + self.bias_current
            recurrent_neurons_firing_rate = torch.zeros((self.recurrent_units, 1), device=device)
            actual_output_signal = torch.zeros(size=(timesteps, self.output_size), device=device)
            for t in range(timesteps):
                R = torch.matmul(self.recurrent_weights, recurrent_neurons_firing_rate)
                I = torch.matmul(self.input_weights, combined_state[:, t]).view(-1, 1)
                recurrent_state = ((recurrent_state * (1 - (1 / self.tau))) + ((1 / self.tau) * (R + I)))
                recurrent_neurons_firing_rate = torch.tanh(recurrent_state)
                actual_output_signal[t] = torch.matmul(self.output_weights, recurrent_neurons_firing_rate).view(-1)
                if t % self.train_dt == 0 or t in actions:
                    if TRAIN_RECURRENT_NEURONS:
                        error = self._error(recurrent_neurons_firing_rate.view(-1), rnn_target[:, t]).view(-1, 1)
                        k_rec = self.P * recurrent_neurons_firing_rate
                        ex_pex = recurrent_neurons_firing_rate.T * k_rec
                        c = 1.0 / (1.0 + ex_pex)
                        self.P -= (k_rec * (k_rec * c))
                        dw = error * k_rec.T * c
                        self.recurrent_weights = self.recurrent_weights - dw
                if t % self.train_dt == 0 or t in actions:
                    if TRAIN_OUTPUT_NEURONS:
                        error = self._error(actual_output_signal[t], output_target[t, 0])
                        k_out = torch.matmul(self.P_out, recurrent_neurons_firing_rate)
                        ex_pex = torch.matmul(recurrent_neurons_firing_rate.T, k_out)
                        c = 1.0 / (1.0 + ex_pex)
                        self.P_out -= torch.matmul(k_out, k_out.T * c)
                        dw = torch.matmul(error, k_out.T * c)
                        self.output_weights = self.output_weights - dw.view(-1)
        # new_activity, rnn_activity = self.forward(combined_state, combined_state.shape[1])
        # plt.plot(activity.view(-1).detach().cpu(), label='actual')
        # plt.plot(output_target.view(-1).detach().cpu(), label='target')
        # plt.plot(new_activity.view(-1).detach().cpu(), label='trained')
        # plt.legend()
        # plt.savefig('rnn_results/{}.png'.format(index))
        # plt.close()
        # return new_activity, activity

    def train_supervised(self, input_signal, output, train_recurrent_weights, train_output_weights):
        timesteps = input_signal.shape[1]
        self.P = torch.zeros((self.recurrent_units, self.recurrent_units), device=device)
        self.P[self.train_rnn_neurons, self.train_rnn_neurons] = self.alpha
        self.P_out = torch.eye(self.recurrent_units, device=device) * self.alpha
        # self.P_out = torch.eye(self.output_size, device=device) * self.alpha
        recurrent_state = torch.zeros(size=(self.recurrent_units, 1), device=device) + self.bias_current
        recurrent_neurons_firing_rate = torch.zeros((self.recurrent_units, 1), device=device)
        actual_output_signal = torch.zeros(size=(timesteps, self.output_size), device=device)
        for t in range(timesteps):
            R = torch.matmul(self.recurrent_weights, recurrent_neurons_firing_rate)
            I = torch.matmul(self.input_weights, input_signal[:, t]).view(-1, 1)
            recurrent_state = ((recurrent_state * (1 - (1 / self.tau))) + ((1 / self.tau) * (R + I)))
            recurrent_neurons_firing_rate = torch.tanh(recurrent_state)
            actual_output_signal[t] = torch.matmul(self.output_weights, recurrent_neurons_firing_rate).view(-1)
            if t % self.train_dt == 0:
                if train_recurrent_weights:
                    error = self._error(recurrent_neurons_firing_rate.view(-1), output[t]).view(-1, 1)
                    k_rec = self.P * recurrent_neurons_firing_rate
                    ex_pex = recurrent_neurons_firing_rate.T * k_rec
                    c = 1.0 / (1.0 + ex_pex)
                    self.P -= (k_rec * (k_rec * c))
                    dw = error * k_rec.T * c
                    self.recurrent_weights = self.recurrent_weights - dw

                if train_output_weights:
                    error = self._error(actual_output_signal[t], output[t])
                    k_out = torch.matmul(self.P_out, recurrent_neurons_firing_rate)
                    ex_pex = torch.matmul(recurrent_neurons_firing_rate.T, k_out)
                    c = 1.0 / (1.0 + ex_pex)
                    self.P_out -= torch.matmul(k_out, k_out.T * c)
                    dw = torch.matmul(error, k_out.T * c)
                    self.output_weights = self.output_weights - dw.view(-1)

    def train_rls(self, input_signal, timepoint, rewards, train_recurrent_weights=True,
                  train_output_weights=True):
        activity = self.get_time(input_signal, input_signal.shape[1])
        timesteps = activity.shape[0]
        _activity, recurrent_neuron_output = self.target_output_and_rnn_activity(input_signal, activity, timepoint,
                                                                                 rewards)
        _activity = torch.clamp(_activity, max=MAX_OUTPUT_CLIP, min=MIN_OUTPUT_CLIP)
        recurrent_neuron_output = torch.clamp(recurrent_neuron_output, min=MIN_RNN_CLIP, max=MAX_RNN_CLIP)
        self.P = torch.zeros((self.recurrent_units, self.recurrent_units), device=device)
        self.P[self.train_rnn_neurons, self.train_rnn_neurons] = self.alpha
        self.P_out = torch.eye(self.recurrent_units, device=device) * self.alpha
        recurrent_state = torch.zeros(size=(self.recurrent_units, 1), device=device) + self.bias_current
        recurrent_neurons_firing_rate = torch.zeros((self.recurrent_units, 1), device=device)
        actual_output_signal = torch.zeros(size=(timesteps, self.output_size), device=device)
        _k = np.array([np.arange(i.item() - 5, i.item() + 5) for i in timepoint]).reshape(-1)
        for t in range(timesteps):
            R = torch.matmul(self.recurrent_weights, recurrent_neurons_firing_rate)
            I = torch.matmul(self.input_weights, input_signal[:, t]).view(-1, 1)
            recurrent_state = ((recurrent_state * (1 - (1 / self.tau))) + ((1 / self.tau) * (R + I)))
            recurrent_neurons_firing_rate = torch.tanh(recurrent_state)
            actual_output_signal[t] = torch.matmul(self.output_weights, recurrent_neurons_firing_rate).view(-1)
            if t % self.train_dt == 0 or t in timepoint:
                if train_recurrent_weights:
                    error = self._error(recurrent_neurons_firing_rate.view(-1), recurrent_neuron_output[:, t]).view(-1,
                                                                                                                    1)
                    k_rec = self.P * recurrent_neurons_firing_rate
                    ex_pex = recurrent_neurons_firing_rate.T * k_rec
                    c = 1.0 / (1.0 + ex_pex)
                    self.P -= (k_rec * (k_rec * c))
                    dw = error * k_rec.T * c
                    self.recurrent_weights = self.recurrent_weights - dw
            if t % self.train_dt == 0 or t in timepoint:
                if train_output_weights:
                    error = self._error(actual_output_signal[t], _activity[t, 0])
                    k_out = torch.matmul(self.P_out, recurrent_neurons_firing_rate)
                    ex_pex = torch.matmul(recurrent_neurons_firing_rate.T, k_out)
                    c = 1.0 / (1.0 + ex_pex)
                    self.P_out -= torch.matmul(k_out, k_out.T * c)
                    dw = torch.matmul(error, k_out.T * c)
                    self.output_weights = self.output_weights - dw.view(-1)

    def _error(self, recurrent_firing_rate, output_signal):
        return recurrent_firing_rate - output_signal

    def load_weights(self, filepath):
        with open('{}/rnn_weights.pik'.format(filepath), 'rb') as io:
            k = pickle.load(io)
        self.input_weights = k['input_weights']
        self.recurrent_weights = k['rnn_weights']
        self.output_weights = k['output_weights']
        self.recurrent_weights = self.recurrent_weights.to(device)
        self.input_weights = self.input_weights.to(device)
        self.output_weights = self.output_weights.to(device)

    def save_weights(self, filepath):
        try:
            with open('{}/rnn_weights.pik'.format(filepath), 'wb') as io:
                pickle.dump({'input_weights': self.input_weights.detach().cpu(),
                             'rnn_weights': self.recurrent_weights.detach().cpu(),
                             'output_weights': self.output_weights.detach().cpu()}, io)
        except:
            print('failed to save weights')
            pass


if __name__ == '__main__':
    state_over_time = np.zeros(4)
    state_over_time[0] = 1
    network = RNNNetwork(input_size=4, recurrent_units=200, output_size=1)
    state = [np.roll(state_over_time, 1) for i in range(2000)]

    for t in range(1, 2000):
        activity = network.get_time(torch.FloatTensor(state_over_time).view(1, 1, -1), 2000)

        state_over_time = np.roll(state_over_time, 1)
        pass
