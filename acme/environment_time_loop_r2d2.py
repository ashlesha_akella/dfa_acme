# python3
# Copyright 2018 DeepMind Technologies Limited. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A simple agent-environment training loop."""

import itertools
import time
import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
from typing import Optional
import numpy as np
from matplotlib import pyplot as plt

from acme import core
# Internal imports.
from acme.utils import counting
from acme.utils import loggers
from collections import namedtuple

from acme.tf import utils as tf2_utils
import dm_env

TOTAL_TIMESTEPS = 2000
INPUT_FOR_MS = 40


class EnvironmentTimeLoopR2D2(core.Worker):
    """A simple RL environment loop.

    This takes `Environment` and `Actor` instances and coordinates their
    interaction. This can be used as:

      loop = EnvironmentLoop(environment, actor)
      loop.run(num_episodes)

    A `Counter` instance can optionally be given in order to maintain counts
    between different Acme components. If not given a local Counter will be
    created to maintain counts between calls to the `run` method.

    A `Logger` instance can also be passed in order to control the output of the
    loop. If not given a platform-specific default logger will be used as defined
    by utils.loggers.make_default_logger. A string `label` can be passed to easily
    change the label associated with the default logger; this is ignored if a
    `Logger` instance is given.
    """

    def __init__(
            self,
            environment: dm_env.Environment,
            actor: core.Actor,
            counter: counting.Counter = None,
            logger: loggers.Logger = None,
            label: str = 'environment_loop',
    ):
        # Internalize agent and environment.
        self._environment = environment
        self._actor = actor
        self._counter = counter or counting.Counter()
        self._logger = logger or loggers.make_default_logger(label)
        self.rnn_memory = namedtuple("Experience", field_names=["state", "action", "reward"])
        self.rnn_buffer = []

    def run(self, num_episodes: Optional[int] = None):
        """Perform the run loop.

        Run the environment loop for `num_episodes` episodes. Each episode is itself
        a loop which interacts first with the environment to get an observation and
        then give that observation to the agent in order to retrieve an action. Upon
        termination of an episode a new episode will be started. If the number of
        episodes is not given then this will interact with the environment
        infinitely.

        Args:
          num_episodes: number of episodes to run the loop for. If `None` (default),
            runs without limit.
        """

        iterator = range(num_episodes) if num_episodes else itertools.count()
        std_time_distribution = 1000
        decay = 0.99999

        for i in iterator:
            # Reset any counts and start the environment.
            start_time = time.time()
            episode_steps = 0
            episode_return = 0
            timestep = self._environment.reset()

            # Make the first observation.
            self._actor.observe_first(timestep)

            if self._actor.time_network is not None:
                # refresh time memory
                self._actor.time_network.refresh()
            std_time_distribution *= decay

            # Run an episode.
            while not timestep.last():
                # Generate an action from the agent's policy and step the environment.
                action = self._actor.select_action(timestep.observation)
                batched_obs = tf2_utils.add_batch_dim(timestep.observation)
                if self._actor.time_network is not None and i % 10 == 0:
                    time_input = self._get_rnn_state(
                        self._actor._actor._network._layers[0]._embed.get_feature(batched_obs))
                    time_to_act = self._actor.time_network.get_time(time_input, TOTAL_TIMESTEPS, with_limit=True)
                    time_to_act = int(np.clip(np.abs(np.random.normal(time_to_act, std_time_distribution)), 0,
                                              TOTAL_TIMESTEPS).item())
                    self._environment._environment._environment._action_repeats = int(time_to_act / 5) + 1
                else:
                    self._environment._environment._environment._action_repeats = 4

                timestep = self._environment.step(action)

                # Have the agent observe the timestep and let the actor update itself.
                self._actor.observe(action, next_timestep=timestep)
                self._actor.update()

                # Book-keeping.
                episode_steps += 1
                episode_return += timestep.reward
                if self._actor.time_network is not None and i % 10 == 0:
                    self.rnn_buffer.append(self.rnn_memory(time_input, time_to_act, timestep[1].item()))

            # Record counts.
            counts = self._counter.increment(episodes=1, steps=episode_steps)

            if self._actor.time_network is not None and i % 10 == 0:
                x = np.array([t.reward for t in self.rnn_buffer]) + 0.00001
                x = tf.linalg.normalize(tf.convert_to_tensor(x))
                x = x[0].numpy()
                try:
                    self._actor.time_network.train_rnn([t.state for t in self.rnn_buffer],
                                                       [t.action for t in self.rnn_buffer],
                                                       x, index=i, iteration=30)
                except:
                    pass
                self.rnn_buffer = []
            # Collect the results and combine with counts.
            steps_per_second = episode_steps / (time.time() - start_time)
            result = {
                'episode_length': episode_steps,
                'episode_return': episode_return,
                'steps_per_second': steps_per_second,
            }
            result.update(counts)

            # Log the given results.
            self._logger.write(result)
            if self._actor.time_network is not None:
                self._actor.time_network.save_weights(self._actor._checkpointer._checkpoint_dir)


    def _get_rnn_state(self, observation):
        _observation = observation[0, :]
        _state = np.zeros((_observation.shape[0], TOTAL_TIMESTEPS))
        _state[:, :INPUT_FOR_MS] = np.repeat(_observation.numpy().reshape(-1, 1), INPUT_FOR_MS, 1)
        return _state
# Internal class.
