## Reward based Hebbian Learning in Direct Feedback Alignment 

Reinforcement learning agents trained using Direct Feedback Alignment and these feedback connections are trained using Reward based Hebbian Learning.

$\Delta B_{i} = E * r * Q_{actual}(s)$

and the DFA using 

$\Delta W_{i}=- ( E * B_i * f^\prime(Q_{actual}(s)) ) \odot  f^\prime(O_{i}) * x_{i}$

To run ``dfa_dqn.py`` with following configurations
 
1. environment = gym.make(**ENV NAME**)
2. network = networks.DFADQN(environment_spec.actions.num_values) for **DFA** 
3. env_loop.run(num_episodes=**Num of episodes**)

Logs are saved with a UUID in ``dfa_logs/dfa/`` folder.