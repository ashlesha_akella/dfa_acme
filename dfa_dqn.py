environment_library = 'gym'

import IPython
import functools
from acme import environment_loop
from acme import specs
from acme import wrappers
from acme.agents.tf import dqn
from acme.tf import networks
from acme.tf import utils as tf2_utils
from acme.utils import loggers
import numpy as np
import sonnet as snt
import tensorflow as tf
import dm_env

# Import the selected environment lib
if environment_library == 'dm_control':
    from dm_control import suite
elif environment_library == 'gym':
    import gym

# Imports required for visualization
# import imageio
# import base64


# Set up a virtual display for rendering.
# display = pyvirtualdisplay.Display(visible=0, size=(1400, 900)).start()

#environment = gym.make('MsPacman-ram-v0')
#environment = gym.make('AirRaid-ram-v0')
environment = gym.make('Centipede-ram-v0')
environment = wrappers.GymWrapper(environment)
environment = wrappers.SinglePrecisionWrapper(environment)

# Grab the spec of the environment.
environment_spec = specs.make_environment_spec(environment)
# num_dimensions = np.prod(environment_spec.actions.shape, dtype=int)

# Create the shared observation network; here simply a state-less operation.
observation_network = tf2_utils.batch_concat
# network = networks.DFADQNAtari(environment_spec.actions.num_values)
network = networks.DQN(environment_spec.actions.num_values)

#network = snt.Sequential([
#    snt.Flatten(),
#    snt.Linear(200),
#    tf.nn.tanh,
#    snt.Linear(100),
#    tf.nn.tanh,
#    snt.Linear(environment_spec.actions.num_values),
    # tf.nn.tanh
#])

# Create a logger for the agent and environment loop.
agent_terminal_logger = loggers.TerminalLogger(label='agent', time_delta=10.)
env_loop_termial_logger = loggers.TerminalLogger(label='env_loop', time_delta=10.)

agent_csv_logger = loggers.CSVLogger(directory='dfa_logs/dqn/Centipede/', label='agent', time_delta=10.)
env_loop_csv_logger = loggers.CSVLogger(directory='dfa_logs/dqn/Centipede/', label='env_loop', time_delta=10.)

agent_logger = loggers.Dispatcher([agent_csv_logger, agent_terminal_logger])
env_loop_logger = loggers.Dispatcher([env_loop_csv_logger, env_loop_termial_logger])

agent = dqn.DQN(
    environment_spec=environment_spec,
    network=network,
    logger=agent_logger,
    checkpoint=False
)

# Create an loop connecting this agent to the environment created above.
env_loop = environment_loop.EnvironmentLoop(environment, agent, logger=env_loop_logger)

# Rerun this cell until the agent has learned the given task.
env_loop.run(num_episodes=10000)


def render(env):
    return env.environment.render(mode='rgb_array')


# test and save the video
#frames = []
#num_steps = 500
#timestep = environment.reset()
#for _ in range(num_steps):
#    frames.append(render(environment))
#    action = agent.select_action(timestep.observation)
#    timestep = environment.step(action)
#
#save_video(frames, filename='Acrobat')
